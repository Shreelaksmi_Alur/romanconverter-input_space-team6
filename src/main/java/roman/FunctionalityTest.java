package roman;

import static org.junit.Assert.*;

import org.junit.Test;

public class FunctionalityTest {

	@Test
	public void toRomanReturnsCorrectRomanNumber() {
		RomanConverter rc = new RomanConverter();
        assertEquals("MMMCMXCIX",rc.toRoman(3999));
	}

	@Test
	public void toRomanReturnsIncorrectRomanNumber() {
		RomanConverter rc = new RomanConverter();
        assertEquals("MMMM",rc.toRoman(4000));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void toRomanFailsForOutOfBoundary() {
		RomanConverter rc = new RomanConverter();
        rc.toRoman(-5);
        fail();
	}
	
	@Test
	public void toRomanPassesForInBoundary() {
		RomanConverter rc = new RomanConverter();
        rc.toRoman(2);
	}
	
	@Test
	public void toRomanShouldBeUppercaseTrue() {
		RomanConverter rc = new RomanConverter();
        assertEquals("C", rc.toRoman(100));
	}
	
	@Test
	public void toRomanShouldBeUppercaseFalse() {
		RomanConverter rc = new RomanConverter();
        assertEquals("m", rc.toRoman(1000));
	}
	
	@Test
	public void fromRomantoIntTrue() {
		RomanConverter rc = new RomanConverter();
        String roman = rc.toRoman(1);
        int romanInt = rc.fromRoman(roman);
        assertEquals(romanInt,1);
	}
	
	@Test
	public void fromRomantoIntFalse() {
		RomanConverter rc = new RomanConverter();
        String roman = rc.toRoman(0);
        int romanInt = rc.fromRoman(roman);
        assertEquals(romanInt,0);
	}
	
	@Test
	public void fromRomanIsRoman(){
		RomanConverter rc = new RomanConverter();
		rc.fromRoman("M");
	}
	
	@Test
	public void fromRomanIsNotRoman(){
		RomanConverter rc = new RomanConverter();
		rc.fromRoman("SAS");
	}
	
	@Test
	public void fromRomanReturnInt(){
		RomanConverter rc = new RomanConverter();
		assertEquals(100,rc.fromRoman("C"));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void fromRomanReturnIntFails(){
		RomanConverter rc = new RomanConverter();
		assertEquals(100,rc.fromRoman("P"));
		fail();
	}
	
	@Test
	public void fromRomanIsUppercase(){
		RomanConverter rc = new RomanConverter();
		rc.fromRoman("C");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void fromRomanIsLowercase(){
		RomanConverter rc = new RomanConverter();
		rc.fromRoman("c");
		fail();
	}
}
