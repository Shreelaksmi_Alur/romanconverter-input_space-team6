package roman;

import static org.junit.Assert.*;

import org.junit.Test;

import roman.RomanConverter;

public class PartitionTest {
	@Test
	public void toRomanEqualToZero(){
		RomanConverter rc = new RomanConverter();
        rc.toRoman(0);
	}

	@Test
	public void toRomanGreaterThanZero(){
		RomanConverter rc = new RomanConverter();
		rc.toRoman(1);
	}
	
	@Test
	public void toRomanLesserThanZero(){
		RomanConverter rc = new RomanConverter();
		rc.toRoman(-1);
	}

	@Test
	public void toRomanEqualToFourThousand(){
		RomanConverter rc = new RomanConverter();
		rc.toRoman(4000);
	}
	
	@Test
	public void toRomanGreaterThanFourThousand(){
		RomanConverter rc = new RomanConverter();
		rc.toRoman(4001);
	}
	
	@Test
	public void toRomanLesserThanFourThousand(){
		RomanConverter rc = new RomanConverter();
		rc.toRoman(3999);
	}
	
	@Test
	public void fromRomanIsEmpty(){
		RomanConverter rc = new RomanConverter();
		rc.fromRoman("");
	}
	
	@Test
	public void fromRomanIsNotEmpty(){
		RomanConverter rc = new RomanConverter();
		rc.fromRoman("SHSH");
	}
	
	@Test
	public void fromRomanIsNull(){
		RomanConverter rc = new RomanConverter();
		rc.fromRoman(null);
	}
	
	@Test
	public void fromRomanIsNotNull(){
		RomanConverter rc = new RomanConverter();
		rc.fromRoman("M");
	}
}
